package com.atguigu.servicebase.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> 自定义异常类</p>
 *
 * @author 江南大学1033190417
 * @date 2022/5/15 22:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuliException extends RuntimeException {

    private Integer code;

    private String msg;
}
