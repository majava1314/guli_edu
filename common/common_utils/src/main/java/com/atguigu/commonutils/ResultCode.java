package com.atguigu.commonutils;

/**
 * @author 江南大学1033190417
 * @date 2022/5/15 19:56
 */
public interface ResultCode {
    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 20001;
}
