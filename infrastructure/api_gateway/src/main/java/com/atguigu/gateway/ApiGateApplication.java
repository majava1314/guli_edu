package com.atguigu.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 江南大学1033190417
 * @date 2022/5/27 17:43
 */

@SpringBootApplication
@EnableDiscoveryClient
public class ApiGateApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiGateApplication.class, args);
    }
}

