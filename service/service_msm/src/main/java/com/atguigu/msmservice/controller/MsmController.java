package com.atguigu.msmservice.controller;

import com.atguigu.msmservice.service.MsmService;
import com.atguigu.msmservice.utils.MsmParam;
import com.atguigu.msmservice.utils.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import com.atguigu.commonutils.R;

/**
 * @author 江南大学1033190417
 * @date 2022/5/28 21:51
 */
@RestController
@RequestMapping("/msmservice")
public class MsmController {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private MsmService msmService;

    @GetMapping("send/{phone}")
    public R sendMsm(@PathVariable("phone") String phone){
        //先从redis获取验证码，如果获取到直接返回
        String code=redisTemplate.opsForValue().get(phone);
        if (!StringUtils.isEmpty(code)){
            return R.ok();
        }
        //如果redis获取不到
        //随机生成值，用于验证码
        code= RandomUtil.getSixBitRandom();//随机生成四位的
        Map<String,Object> param=new HashMap<>();
        param.put("code",code);
        param.put("phone",phone);

        //调用service方法

        boolean isSend=msmService.send(param);

        if (isSend) {
            //将验证码方法redis中,并设置5分钟
            redisTemplate.opsForValue().set(phone,code, MsmParam.EFFECTIVE_TIME, TimeUnit.MINUTES);
            return R.ok().message("发送成功");
        }else {
            return R.error().message("发送失败");
        }
    }
}
