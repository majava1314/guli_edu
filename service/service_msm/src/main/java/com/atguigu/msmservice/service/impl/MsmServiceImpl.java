package com.atguigu.msmservice.service.impl;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.atguigu.msmservice.service.MsmService;
import com.atguigu.msmservice.utils.MsmParam;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author msjava
 * @date 2022/1/7 16:46
 */
@Service
public class MsmServiceImpl implements MsmService {
    @Override
    public boolean send(Map<String, Object> param) {
        String code = (String) param.get("code");
        String phone = (String) param.get("phone");

        if (phone == null || "".equals(phone)) {
            return false;
        }

        DefaultProfile profile = DefaultProfile.getProfile(MsmParam.REGION_ID, MsmParam.ACCESSKEYID, MsmParam.SECRET);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(MsmParam.SYSDOMAIN);
        request.setSysVersion(MsmParam.SYSVERSION);
        request.setSysAction(MsmParam.SYSACTION);
        request.putQueryParameter("SignName", MsmParam.SIGNNAME);
        request.putQueryParameter("TemplateCode", MsmParam.TEMPLATECODE);
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + code + "\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            return response.getHttpResponse().isSuccess();
        } catch (Exception e) {
            return false;
        }
    }
}
