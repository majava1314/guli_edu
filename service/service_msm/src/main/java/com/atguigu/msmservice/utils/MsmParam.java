package com.atguigu.msmservice.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author msjava
 * @date 2022/1/7 21:03
 */

@Component
@RefreshScope//是当前类支持动态刷新
public class MsmParam implements InitializingBean {

    //读取配置文件内容
    @Value("${MSM.REGION_ID}")
    private String region_id;//= "cn-beijing";

    @Value("${MSM.ACCESSKEYID}")
    private String accesskeyid;//= "LTAI5tHYU8aPomRwJntcSDaP";

    @Value("${MSM.SECRET}")
    private String secret;//= "9FhE8mahegxHRxMAlIdazSwVzvi0HW";

    @Value("${MSM.SysDomain}")
    private String sysdomain;//= "dysmsapi.aliyuncs.com";

    @Value("${MSM.SysVersion}")
    private String sysversion;//= "2017-05-25";

    @Value("${MSM.SysAction}")
    private String sysaction;//= "SendSms";

    @Value("${MSM.SignName}")
    private String signname;//= "阿里云短信测试";

    @Value("${MSM.TemplateCode}")
    private String templatecode;//= "SMS_154950909";

    @Value("${MSM.EFFECTIVE_TIME}")
    private Integer effective_time;


    //定义公开的常量
    public static String REGION_ID;
    public static String ACCESSKEYID;
    public static String SECRET;
    public static String SYSDOMAIN;
    public static String SYSVERSION;
    public static String SYSACTION;
    public static String SIGNNAME;
    public static String TEMPLATECODE;
    public static Integer EFFECTIVE_TIME;


    @Override
    public void afterPropertiesSet() throws Exception {
        REGION_ID=region_id;
        ACCESSKEYID=accesskeyid;
        SECRET=secret;
        SYSDOMAIN=sysdomain;
        SYSVERSION=sysversion;
        SYSACTION=sysaction;
        SIGNNAME=signname;
        TEMPLATECODE=templatecode;
        EFFECTIVE_TIME=effective_time;
    }
}
