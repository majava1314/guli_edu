package com.atguigu.msmservice.service;

import java.util.Map;

/**
 * @author msjava
 * @date 2022/1/7 16:46
 */
public interface MsmService {
    boolean send(Map<String, Object> param);
}
