package com.atguigu.cmsservice.controller;

import com.atguigu.cmsservice.entity.CrmBanner;
import com.atguigu.cmsservice.service.CrmBannerService;
import com.atguigu.commonutils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 江南大学1033190417
 * @date 2022/5/28 17:30
 */
@Api(description = "前台")
@RestController
@RequestMapping("/cmsservice/banner")
public class BannerFrontController {

    @Autowired
    private CrmBannerService bannerService;

    @ApiOperation("获取幻灯片")
    @GetMapping("getAllBanner")
    @Cacheable(value = "banner",key = "'allBanner'")
    public R getAllBanner(){
        List<CrmBanner> banners=bannerService.selectAllBanner();
        return R.ok().data("banners",banners);
    }


}
