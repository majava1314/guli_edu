package com.atguigu.cmsservice.controller;


import com.atguigu.cmsservice.entity.CrmBanner;
import com.atguigu.cmsservice.service.CrmBannerService;
import com.atguigu.commonutils.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author ms
 * @since 2022-05-28
 */
@Api(description = "管理员")
@RestController
@RequestMapping("/cmsservice/banner")
public class BannerAdminController {

    @Autowired
    private CrmBannerService bannerService;

    //分页查询
    @GetMapping("pageBanner/{current}/{limit}")
    public R pageBanner(@PathVariable("current") long current,
                        @PathVariable("limit") long limit) {
        Page<CrmBanner> page=new Page<>(current,limit);
        bannerService.page(page,null);
        List<CrmBanner> rows = page.getRecords();
        long total = page.getTotal();
        return R.ok().data("total",total).data("rows",rows);
    }

    //添加banner
    @PostMapping("addBanner")
    public R addBanner(@RequestBody CrmBanner  banner){
        bannerService.save(banner);
        return R.ok();
    }

    @ApiOperation(value = "修改Banner")
    @PutMapping("update")
    public R updateById(@RequestBody CrmBanner banner) {
        bannerService.updateById(banner);
        return R.ok();
    }

    @ApiOperation(value = "删除Banner")
    @DeleteMapping("remove/{id}")
    public R remove(@PathVariable String id) {
        bannerService.removeById(id);
        return R.ok();
    }

    @ApiOperation(value = "获取Banner")
    @GetMapping("get/{id}")
    public R get(@PathVariable String id) {
        CrmBanner banner = bannerService.getById(id);
        return R.ok().data("item", banner);
    }

}

