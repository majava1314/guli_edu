package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.mapper.EduVideoMapper;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.eduservice.service.VodClient;
import com.atguigu.servicebase.exception.GuliException;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author ms
 * @since 2022-05-19
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    private VodClient vodClient;

    @Override
    public boolean removeById(Serializable id) {
        //先查询该小节是否有视频
        EduVideo video = baseMapper.selectById(id);
        if (!StringUtils.isEmpty(video.getVideoSourceId())){
            Boolean success = vodClient.removeVideo(video.getVideoSourceId()).getSuccess();
            if (!success){
                throw new GuliException(20001,"删除视频失败");
            }
        }
        //删除小节
        int i = baseMapper.deleteById(id);
        if (i!=1){
            throw new GuliException(20001,"删除小节失败");
        }
        return true;
    }
}
