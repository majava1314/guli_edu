package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.Subject.OneSubject;
import com.atguigu.eduservice.service.EduSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author ms
 * @since 2022-05-19
 */
@RestController
@RequestMapping("/eduservice/subject")
@Api(description = "课程分类管理")
public class EduSubjectController {

    @Autowired
    private EduSubjectService subjectService;

    @PostMapping("addSubject")
    public R addSubject(MultipartFile file) {
        subjectService.saveSubject(file, subjectService);
        return R.ok();
    }

    @ApiOperation("查询课程分类的树形列表")
    @GetMapping("getAll")
    public R getAll() {
        List<OneSubject> list = subjectService.getAllOneTwoSubject();

        return R.ok().data("list", list);
    }

}

