package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduComment;
import com.atguigu.eduservice.service.EduCommentService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author ms
 * @since 2022-05-30
 */
@Api(description = "课程评论")
@RestController
@RequestMapping("/eduservice/comment")
public class EduCommentController {

    @Autowired
    private EduCommentService commentService;

    @ApiOperation("分页查询评论")
    @GetMapping("page/{current}/{limit}")
    public R page(@PathVariable("current") long current,
                  @PathVariable("limit") long limit,
                  String courseId) {
        Page<EduComment> page = new Page<>(current, limit);
        QueryWrapper<EduComment> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId).orderByDesc("gmt_create");
        commentService.page(page, wrapper);
        Map<String, Object> map = new HashMap<>();
        map.put("items", page.getRecords());
        map.put("current", current);
        map.put("pages", page.getPages());
        map.put("size", page.getSize());
        map.put("total", page.getTotal());
        map.put("hasNext", page.hasNext());
        map.put("hasPrevious", page.hasPrevious());
        return R.ok().data(map);
    }

    @ApiOperation("添加课程评论")
    @PostMapping("addComment")
    public R addComment(@RequestBody EduComment comment) {
        boolean save = commentService.save(comment);
        return R.ok();
    }

}

