package com.atguigu.eduservice.controller.front;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author 江南大学1033190417
 * @date 2022/5/29 19:00
 */
@RestController
@RequestMapping("/eduservice/teacherfront")
public class TeacherFrontController {

    @Autowired
    private EduTeacherService teacherService;

    @Autowired
    private EduCourseService courseService;

    @ApiOperation("分页查询讲师")
    @PostMapping("teacherFrontPage/{current}/{limit}")
    public R teacherFrontPage(@ApiParam("当前页") @PathVariable("current") long current,
                              @PathVariable("limit") long limit) {
        Page<EduTeacher> page = new Page<>(current, limit);
        Map<String, Object> result = teacherService.teacherFrontPage(page);
        return R.ok().data("map", result);
    }

    @ApiOperation("查询讲师信息和课程")
    @GetMapping("getFrontTeacherInfo/{teacherId}")
    public R getFrontTeacherInfo(@ApiParam(value = "讲师id", required = true)
                                 @PathVariable("teacherId") String teacherId) {
        //查询讲师个人信息
        EduTeacher teacher = teacherService.getById(teacherId);

        //查询讲师相关课程
        QueryWrapper<EduCourse> wrapper=new QueryWrapper<>();
        wrapper.eq("teacher_id",teacherId);
        List<EduCourse> courses = courseService.list(wrapper);

        return R.ok().data("teacher",teacher).data("courses",courses);
    }
}
