package com.atguigu.eduservice.controller;

import com.atguigu.commonutils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @author 江南大学1033190417
 * @date 2022/5/17 23:21
 */
@RestController
@RequestMapping("/eduservice/user")
public class EduLoginController {

    @PostMapping("login")
    public R login(){
        return R.ok().data("token","admin");
    }

    @GetMapping("info")
    public R info(){
        return R.ok().data("roles","admin").data("name","name").data("avatar","https://msjava.oss-cn-qingdao.aliyuncs.com/2022/01/08/%E9%BB%91%E5%AE%A2.gif");
    }

}
