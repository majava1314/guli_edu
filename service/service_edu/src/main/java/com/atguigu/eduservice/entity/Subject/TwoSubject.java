package com.atguigu.eduservice.entity.Subject;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 二级分类
 * @author 江南大学1033190417
 * @date 2022/5/19 16:29
 */
@Data
@AllArgsConstructor
public class TwoSubject {
    private String id;

    private String title;

}
