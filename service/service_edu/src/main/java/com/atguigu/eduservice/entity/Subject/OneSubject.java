package com.atguigu.eduservice.entity.Subject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 一级分类
 *
 * @author 江南大学1033190417
 * @date 2022/5/19 16:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OneSubject {

    private String id;

    private String title;

    private List<TwoSubject> children = new ArrayList<>();

    public OneSubject(String id, String title) {
        this.id = id;
        this.title = title;
    }
}
