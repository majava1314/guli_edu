package com.atguigu.eduservice.controller.front;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.chapter.ChapterVo;
import com.atguigu.eduservice.entity.frontvo.CourseFrontVo;
import com.atguigu.eduservice.entity.frontvo.CourseWebVo;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author 江南大学1033190417
 * @date 2022/5/29 19:00
 */
@RestController
@RequestMapping("/eduservice/coursefront")
@Api(description = "前台课程")
public class CourseFrontController {
    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private EduTeacherService teacherService;

    @Autowired
    private EduCourseService courseService;

    @ApiOperation("条件分页查询课程")
    @PostMapping("pageFrontCourse/{current}/{limit}")
    public R pageFrontCourse(@PathVariable("current") long current,
                             @PathVariable("limit") long limit,
                             @RequestBody CourseFrontVo courseFrontVo) {
        Page<EduCourse> page = new Page<>(current, limit);
        Map<String, Object> map = courseService.pageFrontCourse(page, courseFrontVo);
        return R.ok().data(map);
    }

    @ApiOperation("查询课程详情")
    @GetMapping("getFrontCourseInfo/{courseId}")
    public R getFrontCourseInfo(@PathVariable("courseId") String courseId) {
        //查询课程信息
        CourseWebVo courseWebVo = courseService.getBaseCourseInfo(courseId);
        //查询课程章节小节
        List<ChapterVo> chapterVoList = chapterService.getChapterVideoByCourseId(courseId);

        return R.ok().data("courseWebVo", courseWebVo).data("chapterVoList", chapterVoList);
    }
}
