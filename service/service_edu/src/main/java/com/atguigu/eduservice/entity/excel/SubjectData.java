package com.atguigu.eduservice.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author 江南大学1033190417
 * @date 2022/5/19 14:56
 */
@Data
public class SubjectData {
    @ExcelProperty(value = "一级分类", index = 0)
    private String oneSubject;
    @ExcelProperty(value = "二级分类", index = 1)
    private String twoSubject;
}
