package com.atguigu.eduservice.entity.chapter;

import lombok.Data;

/**
 * @author 江南大学1033190417
 * @date 2022/5/20 15:58
 */
@Data
public class VideoVo {
    private String id;

    private String title;

    private String videoSourceId;

}
