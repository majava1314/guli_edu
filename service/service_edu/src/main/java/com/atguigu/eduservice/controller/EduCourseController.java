package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduCourseDescription;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.atguigu.eduservice.service.EduCourseDescriptionService;
import com.atguigu.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author ms
 * @since 2022-05-19
 */
@RestController
@RequestMapping("/eduservice/educourse")
public class EduCourseController {
    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduCourseDescriptionService courseDescriptionService;

    //添加课程基本信息
    @PostMapping("addCourseInfo")
    public R addCourseInfo(@RequestBody CourseInfoVo courseInfo) {
        courseService.saveCourseinfo(courseInfo);
        return R.ok().data("courseId", courseInfo.getId());
    }

    @ApiOperation("根据课程id查询课程信息")
    @GetMapping("getCourseInfo/{courseId}")
    public R getCourseInfo(@PathVariable("courseId") String courseId) {
        EduCourse eduCourse = courseService.getById(courseId);
        CourseInfoVo courseInfoVo = new CourseInfoVo();
        BeanUtils.copyProperties(eduCourse, courseInfoVo);
        EduCourseDescription description = courseDescriptionService.getById(courseId);
        courseInfoVo.setDescription(description.getDescription());
        return R.ok().data("courseInfo", courseInfoVo);
    }

    @ApiOperation("修改课程信息")
    @PostMapping("updateCourseInfo")
    @Transactional
    public R updateCourseInfo(@RequestBody CourseInfoVo courseInfo) {
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfo, eduCourse);
        courseService.updateById(eduCourse);

        EduCourseDescription courseDescription = new EduCourseDescription();
        BeanUtils.copyProperties(courseInfo, courseDescription);
        courseDescriptionService.updateById(courseDescription);
        return R.ok();
    }

    @ApiOperation("根据id查询课程确认信息")
    @GetMapping("getCoursePublishInfo/{courseId}")
    public R getCoursePublishInfo(@PathVariable("courseId") String courseId) {
        CoursePublishVo coursePublish = courseService.getCoursePublishInfo(courseId);
        return R.ok().data("coursePublish", coursePublish);
    }


    //课程最终发布
    @PostMapping("publishCourse/{courseId}")
    public R publishCourse(@PathVariable("courseId") String courseId) {
        EduCourse course = new EduCourse();
        course.setId(courseId).setStatus("Normal");
        courseService.updateById(course);
        return R.ok();
    }

    @ApiOperation("添加查询课程信息")
    @PostMapping("pageCourse/{current}/{limit}")
    public R pageCourse(
            @PathVariable("current") long current,
            @PathVariable("limit") long limit,
            @RequestBody EduCourse queryCourse) {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        String title = queryCourse.getTitle();
        String teacherId = queryCourse.getTeacherId();
        String subjectParentId = queryCourse.getSubjectParentId();
        String subjectId = queryCourse.getSubjectId();
        if (!StringUtils.isEmpty(title)) {
            wrapper.like("title", title);
        }
        if (!StringUtils.isEmpty(teacherId)) {
            wrapper.eq("teacher_id", teacherId);
        }
        if (!StringUtils.isEmpty(subjectParentId)) {
            wrapper.eq("subject_parent_id", subjectParentId);
        }
        if (!StringUtils.isEmpty(subjectId)) {
            wrapper.eq("subject_id", subjectId);
        }
        wrapper.orderByDesc("gmt_modified");
        Page<EduCourse> page = new Page<>(current, limit);
        courseService.page(page, wrapper);
        long total = page.getTotal();
        List<EduCourse> courses = page.getRecords();
        return R.ok().data("total", total).data("rows", courses);
    }

    @ApiOperation("课程删除")
    @DeleteMapping("deleteCourse/{courseId}")
    public R deleteCourse(@PathVariable("courseId") String courseId){
        courseService.deleteCourse(courseId);
        return R.ok();
    }




}

