package com.atguigu.eduservice.service.impl;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.service.VodClient;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * @author 江南大学1033190417
 * @date 2022/5/28 1:28
 */
@Component
public class VodClientImpl implements VodClient {
    //出错只有执行的方法
    @Override
    public R removeVideo(String videoId) {
        return R.error().message("删除视频超时");
    }

    @Override
    public R removeVideoBatch(ArrayList<String> videoIds) {
        return R.error().message("删除多个视频超时");
    }
}
