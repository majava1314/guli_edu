package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author ms
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/eduservice/teacher")
@Api(description = "教师管理")
public class EduTeacherController {

    @Autowired
    private EduTeacherService teacherService;

    @ApiOperation("获得老师列表")
    @GetMapping("getAll")
    public R getAll() {
        List<EduTeacher> teachers = teacherService.list(null);
        return R.ok().data("teachers", teachers);
    }

    @ApiOperation("逻辑删除")
    @DeleteMapping("deleteById/{id}")
    public R deleteById(@ApiParam(name = "id", value = "讲师id", required = true) @PathVariable("id") String id) {
        boolean remove = teacherService.removeById(id);
        if (remove) {
            return R.ok().message("删除成功");
        }
        return R.error().message("删除失败");
    }

    @ApiOperation("条件分页查询")
    @PostMapping("page/{current}/{limit}")
    public R page(@RequestBody EduTeacher teacherQuery,
                  @PathVariable("current") long current,
                  @PathVariable("limit") long limit) {
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        Date begin = teacherQuery.getGmtCreate();
        Date end = teacherQuery.getGmtModified();
        if (!StringUtils.isEmpty(name)) {
            wrapper.like("name", name);
        }
        if (!StringUtils.isEmpty(level)) {
            wrapper.eq("level", level);
        }
        if (!StringUtils.isEmpty(begin)) {
            wrapper.ge("gmt_create", begin);
        }
        if (!StringUtils.isEmpty(end)) {
            wrapper.le("gmt_modified", end);
        }
        wrapper.orderByDesc("gmt_modified");
        Page<EduTeacher> page = new Page<>(current, limit);
        teacherService.page(page, wrapper);
        long total = page.getTotal();
        List<EduTeacher> rows = page.getRecords();
        return R.ok().data("total", total).data("rows", rows);
    }

    @ApiOperation("新增")
    @PostMapping("save")
    public R save(@RequestBody EduTeacher teacher) {
        boolean save = teacherService.save(teacher);
        if (save) {
            return R.ok().message("添加成功");
        }
        return R.error().message("添加失败");
    }


    @ApiOperation("根据id查询")
    @GetMapping("getById/{id}")
    public R getById(@PathVariable("id") String id) {
        EduTeacher teacher = teacherService.getById(id);
        return R.ok().data("teacher", teacher);
    }

    @ApiOperation("根据id修改")
    @PostMapping("update")
    public R update(@RequestBody EduTeacher teacher) {
        boolean update = teacherService.updateById(teacher);
        if (update) {
            return R.ok().message("成功");
        }
        return R.error().message("失败");
    }
}

