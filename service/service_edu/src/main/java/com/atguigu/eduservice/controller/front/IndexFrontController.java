package com.atguigu.eduservice.controller.front;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 江南大学1033190417
 * @date 2022/5/28 17:49
 */
@RestController
@RequestMapping("/eduservice/front")
@Api(description = "前台首页数据")
public class IndexFrontController {

    @Autowired
    private EduTeacherService teacherService;

    @Autowired
    private EduCourseService courseService;


    @ApiOperation("查询前8条热门课程，和前四名讲师")
    @GetMapping("index")
    @Cacheable("index")
    public R index() {
        //查询前八个人们课程
        QueryWrapper<EduCourse> wrapper1 = new QueryWrapper<>();
        wrapper1.orderByDesc("view_count").last("limit 8");
        List<EduCourse> courses = courseService.list(wrapper1);

        //查询前四个讲师
        QueryWrapper<EduTeacher> wrapper2 = new QueryWrapper<>();
        wrapper2.orderByDesc("sort").last("limit 4");
        List<EduTeacher> teachers = teacherService.list(wrapper2);

        return R.ok().data("courses", courses).data("teachers", teachers);
    }
}
