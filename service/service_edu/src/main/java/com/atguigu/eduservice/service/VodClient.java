package com.atguigu.eduservice.service;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.service.impl.VodClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * @author 江南大学1033190417
 * @date 2022/5/28 0:19
 */
@FeignClient(value = "service-vod",fallback = VodClientImpl.class)
@Component
public interface VodClient {

    //删除视频
    @ResponseBody
    @DeleteMapping("/eduvod/video/removeVideo/{videoId}")
    public R removeVideo(@PathVariable("videoId") String videoId);

    //批量删除视频
    @ResponseBody
    @DeleteMapping("/eduvod/video/removeVideoBatch")
    public R removeVideoBatch(@RequestParam("videoIds") ArrayList<String> videoIds);

    }
