package excel;

import com.alibaba.excel.EasyExcel;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 江南大学1033190417
 * @date 2022/5/19 14:27
 */
public class testEasyExcel {
    public static void main(String[] args) {
        //实现写操作
        String fileName = "D:\\桌面\\write.xlsx";
//        EasyExcel.write(fileName, DemoDate.class).sheet("学生列表").doWrite(getData());
        EasyExcel.read(fileName,DemoDate.class,new ExcelListener()).sheet().doRead();

    }

    private static List<DemoDate> getData() {
        List<DemoDate> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DemoDate demoDate = new DemoDate();
            demoDate.setId(i);
            demoDate.setName("姓名"+i);
            list.add(demoDate);
        }
        return list;
    }

    @Test
    public void testRead(){
        EasyExcel.read("D:\\桌面\\write.xlsx",DemoDate.class,new ExcelListener()).sheet().doRead();
    }
}
