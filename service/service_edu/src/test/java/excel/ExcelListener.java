package excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

/**
 * @author 江南大学1033190417
 * @date 2022/5/19 14:35
 */
public class ExcelListener extends AnalysisEventListener<DemoDate> {
    //一行一行的去读取
    @Override
    public void invoke(DemoDate demoDate, AnalysisContext analysisContext) {
        System.out.println("**"+demoDate);
    }

    //读取完成之后
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

    //表头读取
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头"+headMap);
    }
}
