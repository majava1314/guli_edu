package excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author 江南大学1033190417
 * @date 2022/5/19 14:26
 */
@Data
public class DemoDate {
    //设置表头
    @ExcelProperty(value = "学生编号", index = 0)
    private Integer id;

    @ExcelProperty(value = "学生姓名", index = 1)
    private String name;
}
