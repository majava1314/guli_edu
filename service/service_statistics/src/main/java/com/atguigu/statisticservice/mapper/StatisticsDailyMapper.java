package com.atguigu.statisticservice.mapper;

import com.atguigu.statisticservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author ms
 * @since 2022-05-31
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
