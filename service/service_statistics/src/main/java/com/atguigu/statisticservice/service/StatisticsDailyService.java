package com.atguigu.statisticservice.service;

import com.atguigu.statisticservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务类
 * </p>
 *
 * @author ms
 * @since 2022-05-31
 */
public interface StatisticsDailyService extends IService<StatisticsDaily> {

    void registerCount(String day);

    /**
     * 获取图标数据
     * @param type  数据类型
     * @param begin 开始日期
     * @param end   结束日期
     * @return  map数据集
     */
    Map<String, Object> getEchartData(String type, String begin, String end);
}
