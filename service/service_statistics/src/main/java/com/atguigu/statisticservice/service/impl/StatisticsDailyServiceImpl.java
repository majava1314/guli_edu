package com.atguigu.statisticservice.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.atguigu.servicebase.exception.GuliException;
import com.atguigu.statisticservice.entity.StatisticsDaily;
import com.atguigu.statisticservice.mapper.StatisticsDailyMapper;
import com.atguigu.statisticservice.service.StatisticsDailyService;
import com.atguigu.statisticservice.service.UcenterClient;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author ms
 * @since 2022-05-31
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {

    @Autowired
    private UcenterClient ucenterClient;

    @Override
    public void registerCount(String day) {
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.eq("date_calculated", day);
        StatisticsDaily daily = baseMapper.selectOne(wrapper);
        //调用远程接口得到注册人数
        Integer count = (Integer) ucenterClient.registerCount(day).getData().get("count");

        StatisticsDaily statisticsDaily = new StatisticsDaily();
        statisticsDaily.setRegisterNum(count);
        statisticsDaily.setDateCalculated(day);//统计的时间

        //设置其他信息
        // TODO
        statisticsDaily.setVideoViewNum(RandomUtil.randomInt(100, 200));
        statisticsDaily.setLoginNum(RandomUtil.randomInt(100, 200));
        statisticsDaily.setCourseNum(RandomUtil.randomInt(100, 200));
        if (daily == null) {//无则添加
            int insert = baseMapper.insert(statisticsDaily);
            if (insert != 1) {
                throw new GuliException(20001, "统计失败");
            }
        } else {//有则更新
            statisticsDaily.setId(daily.getId());
            int update = baseMapper.updateById(statisticsDaily);
            if (update != 1) {
                throw new GuliException(20001, "统计失败");
            }
        }
    }

    @Override
    public Map<String, Object> getEchartData(String type, String begin, String end) {
        Map<String, Object> map = new HashMap<>();
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.select(type, "date_calculated").between("date_calculated", begin, end).orderByAsc("date_calculated");
        List<StatisticsDaily> dailyList = baseMapper.selectList(wrapper);
        //需要包type，date_calculated转换
        //日期list
        List<String> dataList = dailyList.stream().map(StatisticsDaily::getDateCalculated).collect(Collectors.toList());

        //type数据
        List<Integer> numList = new ArrayList<>();

        switch (type) {
            case "register_num": {
                for (StatisticsDaily d : dailyList) {
                    numList.add(d.getRegisterNum());
                }
                break;
            }
            case "login_num": {
                for (StatisticsDaily d : dailyList) {
                    numList.add(d.getLoginNum());
                }
                break;
            }

            case "video_view_num": {
                for (StatisticsDaily d : dailyList) {
                    numList.add(d.getVideoViewNum());
                }
                break;
            }

            case "course_num": {
                for (StatisticsDaily d : dailyList) {
                    numList.add(d.getCourseNum());
                }
                break;
            }
            default:
                throw new GuliException(20001, "查询不存在数据"+type);
        }
        map.put("dataList", dataList);
        map.put("numList", numList);
        return map;
    }
}
