package com.atguigu.statisticservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.statisticservice.service.StatisticsDailyService;
import com.atguigu.statisticservice.service.UcenterClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author ms
 * @since 2022-05-31
 */
@Api(description = "统计分析")
@RestController
@RequestMapping("/statisticservice/daily")
public class StatisticsDailyController {

    @Autowired
    private StatisticsDailyService dailyService;

    @Autowired
    private UcenterClient ucenterClient;

    @ApiOperation("统计某一天注册人数")
    @GetMapping("registerCount/{day}")
    public R registerCount(@PathVariable("day") String day) {
        dailyService.registerCount(day);
        return R.ok();
    }

    @ApiOperation("图标显示")
    @GetMapping("showData/{type}/{begin}/{end}")
    public R showData(@PathVariable("type") String type,
                      @PathVariable("begin") String begin,
                      @PathVariable("end") String end) {
        Map<String,Object> map=dailyService.getEchartData(type,begin,end);
        return R.ok().data(map);
    }

}

