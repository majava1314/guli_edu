package com.atguigu.statisticservice.service;

import com.atguigu.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 江南大学1033190417
 * @date 2022/5/31 19:38
 */
@FeignClient(value = "service-ucenter")
@Component
public interface UcenterClient {
    /**
     * 统计某一天注册的人数
     *
     * @param day 日期（天）
     * @return 人数
     */
    @ResponseBody
    @GetMapping("/ucenterservice/ucentermember/registerCount/{day}")
    public R registerCount(@PathVariable("day") String day);
}
