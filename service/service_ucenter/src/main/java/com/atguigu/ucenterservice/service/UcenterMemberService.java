package com.atguigu.ucenterservice.service;

import com.atguigu.ucenterservice.entity.UcenterMember;
import com.atguigu.ucenterservice.entity.vo.RegisterVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author ms
 * @since 2022-05-28
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember member);

    /**
     *
     * @param registerVo 注册
     */
    void register(RegisterVo registerVo);

    //查看是否用微信登录
    UcenterMember getByOpenid(String openid);

    /**
     * 统计某一天注册的人数
     * @param day 日期（天）
     * @return  注册人数
     */
    Integer registerCount(String day);
}
