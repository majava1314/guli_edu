package com.atguigu.ucenterservice.controller;


import com.atguigu.commonutils.JwtUtils;
import com.atguigu.commonutils.R;
import com.atguigu.ucenterservice.entity.UcenterMember;
import com.atguigu.ucenterservice.entity.vo.RegisterVo;
import com.atguigu.ucenterservice.service.UcenterMemberService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author ms
 * @since 2022-05-28
 */
@RestController
@RequestMapping("/ucenterservice/ucentermember")
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService memberService;


    @ApiOperation("登录，手机号+密码")
    @PostMapping("login")
    public R login(@RequestBody UcenterMember member) {
        String token = memberService.login(member);
        return R.ok().data("token", token);
    }


    @ApiOperation("注册")
    @PostMapping("register")
    public R register(@RequestBody RegisterVo registerVo){
        memberService.register(registerVo);
        return R.ok();
    }

    @ApiOperation("根据token获取用户信息")
    @GetMapping("getMemberInfo")
    public R getMemberInfo(HttpServletRequest request){
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        UcenterMember member = memberService.getById(memberId);
        return R.ok().data("member",member);
    }

    @ApiOperation("根据id获取用户信息")
    @GetMapping("getUserId/{id}")
    public UcenterMember getUserId(@PathVariable("id") String id){
        return memberService.getById(id);
    }


    @ApiOperation("统计某天注册的人数")
    @GetMapping("registerCount/{day}")
    public R registerCount(@PathVariable("day")String day){
        Integer  count=memberService.registerCount(day);
        return R.ok().data("count",count);
    }

}

