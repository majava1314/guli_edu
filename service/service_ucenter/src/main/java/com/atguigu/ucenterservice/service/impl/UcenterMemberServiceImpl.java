package com.atguigu.ucenterservice.service.impl;

import com.atguigu.commonutils.JwtUtils;
import com.atguigu.commonutils.MD5Util;
import com.atguigu.servicebase.exception.GuliException;
import com.atguigu.ucenterservice.entity.UcenterMember;
import com.atguigu.ucenterservice.entity.vo.RegisterVo;
import com.atguigu.ucenterservice.mapper.UcenterMemberMapper;
import com.atguigu.ucenterservice.service.UcenterMemberService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author ms
 * @since 2022-05-28
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //使用手机号和密码登录
    @Override
    public String login(UcenterMember member) {
        String phone = member.getMobile();
        String password = member.getPassword();

        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", phone);
        UcenterMember ucenterMember = baseMapper.selectOne(wrapper);
        if (ucenterMember == null) {
            throw new GuliException(20001, "手机号不存在");
        }
        if (!ucenterMember.getPassword().equals(MD5Util.getMD5(password))) {
            throw new GuliException(20001, "密码错误");
        }
        //判断用户是否禁用
        if (ucenterMember.getIsDisabled()) {
            throw new GuliException(20001, "用户被禁用");
        }

        //返回token值
        return JwtUtils.getJwtToken(ucenterMember.getId(), ucenterMember.getNickname());
    }

    @Override
    public void register(RegisterVo registerVo) {
        String code = registerVo.getCode();
        String mobile = registerVo.getMobile();
        String password = registerVo.getPassword();

        //判断手机号是否已注册过
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        Integer count = baseMapper.selectCount(wrapper);
        if (count != 0) {
            throw new GuliException(20001, "该手机号已经注册过");
        }


        //判断验证码
        String redisCode = redisTemplate.opsForValue().get(mobile);
        System.out.println(redisCode);
        if (StringUtils.isEmpty(redisCode) || !code.equals(redisCode)) {
            throw new GuliException(20001, "验证码错误");
        }

        //如果验证码无错,删除redis
        redisTemplate.delete(mobile);

        //保存用户
        UcenterMember ucenterMember = new UcenterMember();
        BeanUtils.copyProperties(registerVo, ucenterMember);
        ucenterMember.setPassword(MD5Util.getMD5(password));
        ucenterMember.setAvatar("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.616pic.com%2Fys_bnew_img%2F00%2F12%2F71%2Fu3OyCgBP5v.jpg&refer=http%3A%2F%2Fpic.616pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1656346088&t=cafdfb8ff07732b08616c38202e9ea8d");
        int insert = baseMapper.insert(ucenterMember);
        if (insert != 1) {
            throw new GuliException(20001, "注册失败");
        }
    }

    @Override
    public UcenterMember getByOpenid(String openid) {
        QueryWrapper<UcenterMember> wrapper=new QueryWrapper<>();
        wrapper.eq("openid",openid);
        return baseMapper.selectOne(wrapper);
    }

    @Override
    public Integer registerCount(String day) {
        return baseMapper.registerCount(day);
    }
}
