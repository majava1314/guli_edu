package com.atguigu.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author msjava
 * @date 2021/12/16 17:11
 */
public interface OssService {

    String uploadFileAvatar(MultipartFile file);

}
