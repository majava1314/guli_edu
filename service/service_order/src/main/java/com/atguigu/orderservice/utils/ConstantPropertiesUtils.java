package com.atguigu.orderservice.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 江南大学1033190417
 * @date 2022/5/19 0:27
 */
@Component
public class ConstantPropertiesUtils implements InitializingBean {

    //读取配置文件中的内容
    @Value("${weixin.pay.appid}")
    private String appid;

    @Value("${weixin.pay.partner}")
    private String partner;

    @Value("${weixin.pay.partnerkey}")
    private String partnerkey;

    @Value("${weixin.pay.notifyurl}")
    private String notifyurl;

    //把上述变量初始化之后会执行此方法

    public static String APP_ID;

    public static String PARTNER;

    public static String PARTNER_KEY;

    public static String NOTIFY_URL;


    @Override
    public void afterPropertiesSet() throws Exception {
        APP_ID=appid;
        PARTNER=partner;
        PARTNER_KEY=partnerkey;
        NOTIFY_URL=notifyurl;
    }
}
