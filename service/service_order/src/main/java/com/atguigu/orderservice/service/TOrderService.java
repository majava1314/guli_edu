package com.atguigu.orderservice.service;

import com.atguigu.orderservice.entity.TOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author ms
 * @since 2022-05-30
 */
public interface TOrderService extends IService<TOrder> {

    /**
     * 创建订单返回订单号
     * @param courseId  课程id
     * @param memberId  用户id
     * @return
     */
    String createOrder(String courseId, String memberId);
}
