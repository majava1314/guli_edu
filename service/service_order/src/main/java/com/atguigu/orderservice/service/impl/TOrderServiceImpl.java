package com.atguigu.orderservice.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.atguigu.orderservice.entity.TOrder;
import com.atguigu.orderservice.entity.vo.CourseWebVo;
import com.atguigu.orderservice.entity.vo.UcenterMember;
import com.atguigu.orderservice.mapper.TOrderMapper;
import com.atguigu.orderservice.service.EduClient;
import com.atguigu.orderservice.service.TOrderService;
import com.atguigu.orderservice.service.UcenterClient;
import com.atguigu.orderservice.utils.OrderNoUtil;
import com.atguigu.servicebase.exception.GuliException;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author ms
 * @since 2022-05-30
 */
@Service
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements TOrderService {

    @Autowired
    private UcenterClient ucenterClient;

    @Autowired
    private EduClient eduClient;


    @Override
    public String createOrder(String courseId, String memberId) {
        //远程调用获取用户信息
        UcenterMember member = ucenterClient.getUserId(memberId);

        //远程调用获取课程信息
        CourseWebVo course = BeanUtil.toBean(eduClient.getFrontCourseInfo(courseId).getData().get("courseWebVo"), CourseWebVo.class);

        TOrder order=new TOrder();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(course.getTitle());
        order.setCourseCover(course.getCover());
        order.setTeacherName(course.getTeacherName());
        order.setMemberId(memberId);
        order.setNickname(member.getNickname());
        order.setMobile(member.getMobile());
        order.setTotalFee(course.getPrice());
        order.setStatus(0);//默认未支付
        order.setPayType(1);//默认微信支付
        int insert = baseMapper.insert(order);
        if (insert!=1){
            throw new GuliException(20001,"创建订单失败");
        }
        return order.getOrderNo();
    }
}
