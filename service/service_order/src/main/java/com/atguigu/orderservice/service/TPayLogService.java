package com.atguigu.orderservice.service;

import com.atguigu.orderservice.entity.TPayLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author ms
 * @since 2022-05-30
 */
public interface TPayLogService extends IService<TPayLog> {

    /**
     * 生成支付二维码
     * @param orderNo  订单号
     * @return
     */
    Map<String, Object> createNative(String orderNo);

    /**
     * 查询订单状态
     * @param orderNo
     * @return
     */
    Map<String, String> queryPayStatus(String orderNo);

    /**
     * 更改订单状态,添加支付记录到表
     * @param map
     */
    void updateOrderStatus(Map<String, String> map);
}
