package com.atguigu.orderservice.service;

import com.atguigu.orderservice.entity.vo.UcenterMember;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 江南大学1033190417
 * @date 2022/5/30 23:25
 */
@FeignClient(value = "service-ucenter")
@Component
public interface UcenterClient {

    @ResponseBody
    @GetMapping("/ucenterservice/ucentermember/getUserId/{id}")
    public UcenterMember getUserId(@PathVariable("id") String id);


}
