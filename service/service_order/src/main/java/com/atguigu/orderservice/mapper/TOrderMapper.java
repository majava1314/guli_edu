package com.atguigu.orderservice.mapper;

import com.atguigu.orderservice.entity.TOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author ms
 * @since 2022-05-30
 */
public interface TOrderMapper extends BaseMapper<TOrder> {

}
