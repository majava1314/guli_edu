package com.atguigu.orderservice.service;

import com.atguigu.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 江南大学1033190417
 * @date 2022/5/30 23:27
 */
@FeignClient(value = "service-edu")
@Component
public interface EduClient {

    @ResponseBody
    @GetMapping("/eduservice/coursefront/getFrontCourseInfo/{courseId}")
    public R getFrontCourseInfo(@PathVariable("courseId") String courseId);
}
