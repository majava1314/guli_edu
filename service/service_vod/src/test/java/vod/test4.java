package vod;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;

import static vod.test.initVodClient;

/**
 * @author 江南大学1033190417
 * @date 2022/5/27 1:43
 */
public class test4 {

    /**
     * 删除视频
     * @param client 发送请求客户端
     * @return DeleteVideoResponse 删除视频响应数据
     * @throws Exception
     */
    public static DeleteVideoResponse deleteVideo(DefaultAcsClient client) throws Exception {
        DeleteVideoRequest request = new DeleteVideoRequest();
        //支持传入多个视频ID，多个用逗号分隔
        request.setVideoIds("45c5d8bf14fe4c63aca8ea2a89f36bdc");
        return client.getAcsResponse(request);
    }

    /*请求示例*/
    public static void main(String[] argv) throws ClientException {
        DefaultAcsClient client = initVodClient("LTAI5tHYU8aPomRwJntcSDaP", "9FhE8mahegxHRxMAlIdazSwVzvi0HW");
        DeleteVideoResponse response = new DeleteVideoResponse();
        try {
            response = deleteVideo(client);
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
        }
        System.out.print("RequestId = " + response.getRequestId() + "\n");
    }
}
