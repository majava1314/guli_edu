package com.atguigu.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author 江南大学1033190417
 * @date 2022/5/27 2:02
 */
public interface VodService {

    //上传是视频
    String uploadVideo(MultipartFile file);

    //根据视频id删除视频
    void deleteVideo(String videoId);

    //批量删除视频
    void removeVideoBatch(List<String> videoIds);

    /**
     * 根据视频id获取视频播放凭证
     * @param videoId  视频id
     * @return
     */
    String getPlayAuth(String videoId);
}
