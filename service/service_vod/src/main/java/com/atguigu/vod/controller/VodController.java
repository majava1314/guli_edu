package com.atguigu.vod.controller;

import com.atguigu.commonutils.R;
import com.atguigu.vod.service.VodService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 江南大学1033190417
 * @date 2022/5/27 2:02
 */
@RestController
@RequestMapping("/eduvod/video")
public class VodController {

    @Autowired
    private VodService vodService;

    @PostMapping("uploadVideo")
    public R uploadVideo(MultipartFile file) {
        String videoId = vodService.uploadVideo(file);
        return R.ok().data("videoId", videoId);
    }

    @DeleteMapping("removeVideo/{videoId}")
    public R removeVideo(@PathVariable("videoId") String videoId) {
        vodService.deleteVideo(videoId);
        return R.ok();
    }

    @ApiOperation("批量删除视频")
    @DeleteMapping("removeVideoBatch")
    public R removeVideoBatch(@RequestParam("videoIds") ArrayList<String> videoIds){
        System.out.println(videoIds);
        vodService.removeVideoBatch(videoIds);
        return R.ok();
    }

    @ApiOperation("根据视频id获取视频播放凭证")
    @GetMapping("getVideoPlayAuth/{videoId}")
    public R getVideoPlayAuth(@PathVariable("videoId") String videoId){
        String playAuth = vodService.getPlayAuth(videoId);

        return R.ok().data("playAuth",playAuth);
    }
}
